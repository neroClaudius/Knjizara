package vp.spring.knjizara;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjekarKnjizaraApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjekarKnjizaraApplication.class, args);
	}
}
