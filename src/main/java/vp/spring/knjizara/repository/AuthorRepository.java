package vp.spring.knjizara.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import vp.spring.knjizara.model.Author;

@Component
public interface AuthorRepository extends JpaRepository<Author, Long> {

	

	@Query(value = "select a.name,a.surname,count(ab.authors_id)as broj from author a left "
			+ "join  author_books ab on a.id = ab.authors_id group by ab.authors_id order by broj asc", nativeQuery = true)
	public List<Object[]> displayAuthorsByNumberOfWorks();
	
	
	@Query(value="select distinct a.name,a.surname from author a  join author_books ab on a.id=ab.authors_id \r\n" + 
			"						join book b on ab.books_id = b.id where \r\n" + 
			"                        b.genre_id = :id",nativeQuery = true)
	public List<Object[]> displayAuthorsByGenre(@Param("id") Long genreId);
	
	
}
