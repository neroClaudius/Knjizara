package vp.spring.knjizara.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import vp.spring.knjizara.model.Book;
@Component
public interface BookRepository extends JpaRepository<Book, Long>{

}
