package vp.spring.knjizara.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import vp.spring.knjizara.model.Genre;


@Component
public interface GenreRepository extends JpaRepository<Genre, Long>{

}
