package vp.spring.knjizara.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import vp.spring.knjizara.model.Book;
import vp.spring.knjizara.repository.BookRepository;

@Component
public class BookService {

	@Autowired
	BookRepository bookRepository;

	public Book findOne(Long id) {
		return bookRepository.findOne(id);
	}

	public List<Book> findAll() {
		return bookRepository.findAll();
	}

	public Page<Book> findAll(Pageable page) {
		return bookRepository.findAll(page);
	}

	public Book save(Book book) {
		return bookRepository.save(book);
	}

	public void delete(Long id) {
		bookRepository.delete(id);
	}

}
