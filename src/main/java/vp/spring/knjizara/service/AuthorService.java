package vp.spring.knjizara.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import vp.spring.knjizara.model.Author;
import vp.spring.knjizara.repository.AuthorRepository;

@Component
public class AuthorService {

	@Autowired
	AuthorRepository authorRepository;

	public List<Author> findAll() {
		return authorRepository.findAll();
	}

	public Author findOne(Long id) {
		return authorRepository.findOne(id);
	}

	public Page<Author> findAll(Pageable page) {
		return authorRepository.findAll(page);
	}

	public Author save(Author author) {
		return authorRepository.save(author);
	}

	public void delete(Long id) {
		authorRepository.delete(id);
	}

	public List<Object[]> displayAuthorsByNumberOfWorks() {
		return authorRepository.displayAuthorsByNumberOfWorks();
	}

	public List<Object[]> displayAuthorsByGenre(Long genreId) {
		return authorRepository.displayAuthorsByGenre(genreId);
	}
}
