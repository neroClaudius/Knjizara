package vp.spring.knjizara.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.knjizara.model.Author;
import vp.spring.knjizara.model.Book;
import vp.spring.knjizara.service.BookService;
import vp.spring.knjizara.web.dto.AuthorDTO;
import vp.spring.knjizara.web.dto.BookDTO;
import vp.spring.knjizara.web.dto.GenreDTO;

@RestController
public class BookController {

	@Autowired
	BookService bookService;

	@RequestMapping(value = "api/books/{id}", method = RequestMethod.GET)
	public ResponseEntity<BookDTO> findOneBook(@PathVariable Long id) {

		Book b = bookService.findOne(id);

		if (b == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		BookDTO bDTO = new BookDTO();
		bDTO.setId(b.getId());
		bDTO.setGenre(new GenreDTO(b.getGenre()));
		List<AuthorDTO> autDTO = new ArrayList<>();

		for (Author a : b.getAuthors()) {
			autDTO.add(new AuthorDTO(a));
		}

		bDTO.setAuthors(autDTO);

		return new ResponseEntity<>(bDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "api/books", method = RequestMethod.GET)
	public ResponseEntity<List<BookDTO>> findAll(Pageable page) {

		Page<Book> pages = bookService.findAll(page);

		HttpHeaders headers = new HttpHeaders();
		long studentsTotal = pages.getTotalElements();
		headers.add("X-Total-Count", String.valueOf(studentsTotal));

		List<BookDTO> retVal = convert(pages.getContent());

		return new ResponseEntity<>(retVal, HttpStatus.OK);

	}

	public List<BookDTO> convert(List<Book> books) {
		List<BookDTO> retVal = new ArrayList<>();

		for (Book b : books) {
			BookDTO bDTO = new BookDTO();
			bDTO.setId(b.getId());
			bDTO.setTitle(b.getTitle());
			bDTO.setGenre(new GenreDTO(b.getGenre()));
			for (Author a : b.getAuthors()) {
				bDTO.getAuthors().add(new AuthorDTO(a));
			}
			retVal.add(bDTO);
		}
		return retVal;
	}

}
