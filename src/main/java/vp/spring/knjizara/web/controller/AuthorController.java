package vp.spring.knjizara.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.knjizara.model.Author;
import vp.spring.knjizara.service.AuthorService;
import vp.spring.knjizara.web.dto.AuthorDTO;

@RestController
public class AuthorController {

	@Autowired
	AuthorService authorService;

	@RequestMapping(value = "api/authors/{id}", method = RequestMethod.GET)
	public ResponseEntity<AuthorDTO> findOneAuthor(@PathVariable Long id) {

		Author a = authorService.findOne(id);

		if (a == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(new AuthorDTO(a), HttpStatus.OK);
		}
	}

	@RequestMapping(value = "api/authors", method = RequestMethod.GET)
	public ResponseEntity<List<AuthorDTO>> findAll(Pageable page) {

		Page<Author> authors = authorService.findAll(page);
		List<AuthorDTO> retVal = new ArrayList<>();

		for (Author a : authors) {
			retVal.add(new AuthorDTO(a));
		}

		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}

	@RequestMapping(value = "api/authors", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AuthorDTO> createAuthor(@RequestBody AuthorDTO authorDTO) {

		Author a = new Author();
		a.setId(authorDTO.getId());
		a.setName(authorDTO.getName());
		a.setSurname(authorDTO.getSurname());

		a = authorService.save(a);

		return new ResponseEntity<>(new AuthorDTO(a), HttpStatus.CREATED);
	}

	@RequestMapping(value = "api/authors/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AuthorDTO> modifyAuthor(@RequestBody AuthorDTO authorDTO, @PathVariable Long id) {

		Author a = authorService.findOne(id);

		if (a == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		a.setId(authorDTO.getId());
		a.setName(authorDTO.getName());
		a.setSurname(authorDTO.getSurname());

		return new ResponseEntity<>(new AuthorDTO(a), HttpStatus.OK);
	}

	@RequestMapping(value = "api/authors/by", method = RequestMethod.GET)
	public ResponseEntity<List<Object[]>> doSomethind() {
		List<Object[]> ff = authorService.displayAuthorsByNumberOfWorks();

		return new ResponseEntity<>(ff, HttpStatus.OK);
	}

	@RequestMapping(value = "api/books/{genreId}/authors")
	public ResponseEntity<List<Object[]>> findByGenreId(@PathVariable Long genreId) {

		List<Object[]> list = authorService.displayAuthorsByGenre(genreId);

		return new ResponseEntity<>(list, HttpStatus.OK);

	}

}
