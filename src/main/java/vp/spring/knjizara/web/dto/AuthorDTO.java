package vp.spring.knjizara.web.dto;

import vp.spring.knjizara.model.Author;

public class AuthorDTO {

	private Long id;
	private String name;
	private String surname;

	public AuthorDTO() {

	}

	public AuthorDTO(Author author) {
		this.id = author.getId();
		this.name = author.getName();
		this.surname = author.getSurname();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

}
