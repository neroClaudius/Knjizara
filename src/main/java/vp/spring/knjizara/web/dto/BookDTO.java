package vp.spring.knjizara.web.dto;

import java.util.ArrayList;
import java.util.List;

import vp.spring.knjizara.model.Book;

public class BookDTO {

	private Long id;
	private String title;
	private GenreDTO genre;
	private List<AuthorDTO> authors = new ArrayList<AuthorDTO>();

	public BookDTO() {

	}

	public BookDTO(Book book) {
		this.id = book.getId();
		this.title = book.getTitle();
		this.genre = new GenreDTO(book.getGenre());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public GenreDTO getGenre() {
		return genre;
	}

	public void setGenre(GenreDTO genre) {
		this.genre = genre;
	}

	public List<AuthorDTO> getAuthors() {
		return authors;
	}

	public void setAuthors(List<AuthorDTO> authors) {
		this.authors = authors;
	}

}
