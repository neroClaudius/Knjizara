package vp.spring.knjizara.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "bookstore", name = "genre")
public class Genre {

	@Id
	@GeneratedValue
	private Long id;
	private String name;

	public Genre(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Genre() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
